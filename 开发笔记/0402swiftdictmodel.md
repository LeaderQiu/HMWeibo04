# SwiftDictModel

## 新建 SwiftDictModel 文件

新建 `SwiftDictModel` 负责字典转模型

## 目标一 －了解类信息

```swift
func modelInfo(cls: AnyClass) -> [String: String]? {
    return nil
}
```

在 `ViewController` 中添加测试代码

```swift
let manager = SwiftDictModel.sharedManager

override func viewDidLoad() {
    super.viewDidLoad()

    println(manager.modelInfo(Model.self))
}
```

### 使用 `ivars(成员变量)`

```swift
// 拷贝成员变量
var count: UInt32 = 0
let ivars = class_copyIvarList(cls, &count)
println(count)

for i in 0..<count {
    // 变量名
    let ivar = ivars[Int(i)]
    let cname = ivar_getName(ivar)
    let name = String.fromCString(cname)

    // 属性
    let ctype = ivar_getTypeEncoding(ivar)
    let type = String.fromCString(ctype)

    println("\(name) --- \(type)")
}

// 释放
free(ivars)
```

> 测试结果：能够获取模型的所有属性，但是无法获取对应的类型

### 使用 `properties(属性)`

```swift
// 拷贝属性
var count: UInt32 = 0
let properties = class_copyPropertyList(cls, &count)
println(count)

for i in 0..<count {
    // 变量名
    let property = properties[Int(i)]
    let cname = property_getName(property)
    let name = String.fromCString(cname)

    // 属性
    let ctype = property_getAttributes(property)
    let type = String.fromCString(ctype)

    println("\(name) --- \(type)")
}

// 释放
free(properties)
```

> 测试结果：能够获取属性类型，但是存在两个问题

1. 基本数据类型，如果是可选的无法获取
2. 如果是数组，同样无法获取数组中的对象信息

### 解决思路

#### 自定义协议

```swift
protocol DictModelProtocol {
    ///  自定义类映射字典
    ///
    ///  :returns: 可选映射字典
    static func customClassMapping() -> [String: String]?
}
```

#### 在模型类中添加协议方法实现

```swift
///  测试模型
class Model: NSObject, DictModelProtocol {
    var str1: String?
    var str2: NSString?
    var b: Bool?
    var i: Int = 0
    var f: Float?
    var d: Double?
    var num: NSNumber?
    var namelist: [String]?
    var info: Info?
    var other: [Info]?
    var others: NSArray?

    static func customClassMapping() -> [String : String]? {
        return ["info": "Info", "other": "Info", "others": "Info"]
    }
}
```

#### 检测类是否实现了协议方法

```swift
// 检查类是否实现了协议
var mappingDict: [String: String]?
if cls.respondsToSelector("customClassMapping") {
    mappingDict = cls.customClassMapping()
}
```

注意，如果要让类能够调用协议方法，需要在协议声明前增加 `@objc`

```swift
@objc protocol DictModelProtocol
```

#### 生成类的模型数据

```swift
///  加载类信息
///
///  :param: cls 模型类
///
///  :returns: 返回模型类信息
func modelInfo(cls: AnyClass) -> [String: String] {
    // 拷贝成员变量
    var count: UInt32 = 0
    let ivars = class_copyIvarList(cls, &count)

    // 检查类是否实现了协议
    var mappingDict: [String: String]?
    if cls.respondsToSelector("customClassMapping") {
        mappingDict = cls.customClassMapping()
    }

    var infoDict = [String: String]()
    for i in 0..<count {
        // 变量名
        let ivar = ivars[Int(i)]
        let cname = ivar_getName(ivar)
        let name = String.fromCString(cname)!

        let type = mappingDict?[name] ?? ""

        infoDict[name] = type
    }

    // 释放
    free(ivars)

    return infoDict
}
```
