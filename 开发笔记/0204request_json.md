# 请求 JSON

## 错误处理

```swift
// 如果网络请求没有创建成功，应该生成一个错误，提供给其他的开发者
/**
    domain: 错误所属领域字符串 com.itheima.error
    code: 如果是复杂的系统，可以自己定义错误编号
    userInfo: 错误信息字典
*/
let error = NSError(domain: SimpleNetwork.errorDomain, code: -1, userInfo: ["error": "请求建立失败"])
completion(result: nil, error: error)
```

### errorDomain 常量
```swift
// 静态属性，在 Swift 中类属性可以返回值但是不能存储数值
static let errorDomain = "com.itheima.error"
```

## 请求 JSON

```swift
// 实例化网络请求
if let request = request(method, urlString, params) {

    // 访问网络 － 本身的回调方法是异步的
    session!.dataTaskWithRequest(request, completionHandler: { (data, _, error) -> Void in

        // 如果有错误，直接回调，将网络访问的错误传回
        if error != nil {
            completion(result: nil, error: error)
            return
        }

        // 反序列化 -> 字典或者数组
        let json: AnyObject? = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: nil)

        // 判断是否反序列化成功
        if json == nil {
            let error = NSError(domain: SimpleNetwork.errorDomain, code: -1, userInfo: ["error": "反序列化失败"])
            completion(result: nil, error: error)
        } else {
            // 有结果
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                completion(result: json, error: nil)
            })
        }
    }).resume()

    return
}
```
