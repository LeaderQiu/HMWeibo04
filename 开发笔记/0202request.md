# 网络请求

## GET & POST

* GET 的参数包含在 URL 中
* POST 的参数包含在数据体中

为了方便后续网络请求的调用，定义一个函数，根据网络访问参数实例化网络请求

```swift
///  返回网络访问的请求
///
///  :param: method    HTTP 访问方法
///  :param: urlString urlString
///  :param: params    可选参数字典
///
///  :returns: 可选网络请求
func request(method: HTTPMethod, _ urlString: String, _ params: [String: String]?) -> NSURLRequest? {

    // isEmpty 是 "" & nil
    if urlString.isEmpty {
        return nil
    }

    // 记录 urlString，因为传入的参数是不可变的
    var urlStr = urlString
    var r: NSMutableURLRequest?

    if method == .GET {
        // URL 的参数是拼接在URL字符串中的
        // 1. 生成查询字符串
        let query = queryString(params)

        // 2. 如果有拼接参数
        if query != nil {
            urlStr += "?" + query!
        }

        // 3. 实例化请求
        r = NSMutableURLRequest(URL: NSURL(string: urlStr)!)
    } else {

        // post 请求必须要有提交给服务器的数据体
        if let query = queryString(params) {
            r = NSMutableURLRequest(URL: NSURL(string: urlStr)!)

            // 设置请求方法
            // swift 语言中，枚举类型，如果要去的返回值，需要使用一个 rawValue
            r!.HTTPMethod = method.rawValue

            // 设置数据体
            r!.HTTPBody = query.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        }
    }

    return r
}

```
