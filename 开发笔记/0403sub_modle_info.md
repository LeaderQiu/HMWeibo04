# 子类信息

## 目标二 －子类信息

### 定义子类模型

```swift
class SubModel: Model {
    var school: String?
}
```

### 测试效果

```
manager.modelInfo(SubModel.self)
```

> 测试结果：只能获取子类自身的属性信息

### 解决思路

> 在获取类信息的时候，查询父类属性

#### 准备方法

```swift
///  加载完整类信息
///
///  :param: cls 模型类
///
///  :returns: 模型类完整信息
func fullModelInfo(cls: AnyClass) -> [String: String] {

    return ["": ""]
}
```

#### 遍历继承顺序

```swift
var currentCls: AnyClass = cls
while let parent: AnyClass = currentCls.superclass() {
    println(currentCls)
    currentCls = parent
}
```

#### 字典扩展

为了简化字典合并操作，增加字典扩展

```swift
extension Dictionary {
    mutating func merge(dict: [String: String]) {
        for (k, v) in dict {
            self.updateValue(v as! Value, forKey: k as! Key)
        }
    }
}
```

**注意**在扩展中，需要明确指明 `key` 和 `value`

##### 泛型

```swift
extension Dictionary {
    ///  将字典合并到当前字典
    mutating func merge<K, V>(dict: [K: V]) {
        for (k, v) in dict {
            self.updateValue(v as! Value, forKey: k as! Key)
        }
    }
}
```

#### 合并字典

```swift
///  加载完整类信息
///
///  :param: cls 模型类
///
///  :returns: 模型类完整信息
func fullModelInfo(cls: AnyClass) -> [String: String] {

    var currentCls: AnyClass = cls

    var infoDict = [String: String]()
    while let parent: AnyClass = currentCls.superclass() {
        infoDict.merge(modelInfo(currentCls))
        currentCls = parent
    }

    return infoDict
}
```


