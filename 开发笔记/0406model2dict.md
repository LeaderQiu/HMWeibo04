# 模型转字典

## KVC 取值

```swift
func objectDictionary(obj: AnyObject) -> [String: AnyObject] {
    // 1. 取出对象模型字典
    let infoDict = fullModelInfo(obj.classForCoder)

    var result = [String: AnyObject]()
    // 2. 遍历字典
    for (k, v) in infoDict {
        // 取值
        var value: AnyObject? = obj.valueForKey(k)
        if value == nil {
            value = NSNull()
        }

        if v.isEmpty {
            result[k] = value
        } else {
            println("自定义对象 \(k) \(v)")
        }
    }

    return result
}
```

## 自定义对象

```swift
let type = "\(value!.classForCoder)"
println("自定义对象 \(k) \(v) \(type)")

if type == "NSArray" {

} else {
    // 自定义对象
    result[k] = objectDictionary(value!)
}
```

## 自定义对象数组

### 定义函数

```swift
///  模型数组转字典数组
///
///  :param: array 模型数组
///
///  :returns: 字典数组
func objectArray(array: [AnyObject]) -> [AnyObject]? {

    return nil
}
```

### 代码实现

```swift
///  模型数组转字典数组
///
///  :param: array 模型数组
///
///  :returns: 字典数组
func objectArray(array: [AnyObject]) -> [AnyObject]? {

    var result = [AnyObject]()

    for value in array {
        let type = "\(value.classForCoder)"

        var subValue: AnyObject?
        if type == "NSArray" {
            subValue = objectArray(value as! [AnyObject])
        } else {
            subValue = objectDictionary(value)
        }
        if subValue != nil {
            result.append(subValue!)
        }
    }

    if result.count > 0 {
        return result
    } else {
        return nil
    }
}
```

### 调整字典转模型代码

```swift
///  模型转字典
///
///  :param: obj 模型对象
///
///  :returns: 字典信息
func objectDictionary(obj: AnyObject) -> [String: AnyObject]? {
    // 1. 取出对象模型字典
    let infoDict = fullModelInfo(obj.classForCoder)

    var result = [String: AnyObject]()
    // 2. 遍历字典
    for (k, v) in infoDict {
        // 取值
        var value: AnyObject? = obj.valueForKey(k)
        if value == nil {
            value = NSNull()
        }

        if v.isEmpty || value === NSNull() {
            result[k] = value
        } else {
            let type = "\(value!.classForCoder)"
            println("自定义对象 \(k) \(v) \(type)")

            var subValue: AnyObject?
            if type == "NSArray" {
                subValue = objectArray(value! as! [AnyObject])
            } else {
                subValue = objectDictionary(value!)
            }
            if subValue == nil {
                subValue = NSNull()
            }
            result[k] = subValue
        }
    }

    if result.count > 0 {
        return result
    } else {
        return nil
    }
}
```

