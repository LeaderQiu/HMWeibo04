# 字典转模型

## 定义函数

```
///  字典转模型
///
///  :param: dict 数据字典
///  :param: cls  模型类
///
///  :returns: 模型对象
func objectWithDictionary(dict: NSDictionary, cls: AnyClass) -> AnyObject? {
    return nil
}
```

### 第一步 - println确认思路

```swift
// 1. 模型信息
let infoDict = fullModelInfo(cls)

// 2. 实例化对象
let obj: AnyObject = cls.alloc()

// 3. 遍历模型字典
for (k, v) in infoDict {
    if let value: AnyObject = dict[k] {
        if v.isEmpty {
            println("KVC设置数值 \(k) \(value)")
        } else {
            println("查找子对象 \(k) \(v)")
        }
    }
}
```

### 第二步 - kvc

```swift
if v.isEmpty {
    println("KVC设置数值 \(k) \(value)")
    obj.setValue(value, forKey: k)
} else {
    println("查找子对象 \(k) \(v)")
}
```

测试结果

* 基本数据类型不能是可选值 ?
* null 给基本数据类型赋值会崩

解决办法

* 约定所有基础数据类型不要用 ?
* 如果 json 中包含的是 null，则不设置数值

```swift
if !(value === NSNull()) {
    obj.setValue(value, forKey: k)
}
```

#### 跟踪对象信息

在模型中添加如下方法，跟踪模型信息

```swift
extension Model: DebugPrintable {
    override var debugDescription: String {
        let keys = ["str1", "str2", "b", "i", "f", "d", "num", "namelist"]

        return "\(self.dictionaryWithValuesForKeys(keys))"
    }
}
```

### 第三步 自定义对象

#### println 确认思路

```swift
// 获取 value 的数据类型(字典/数组)
let type = "\(value.classForCoder)"

println("查找子对象 \(k) \(v)" + type)
if type == "NSDictionary" {
    println("\t自定义对象")
} else if type == "NSArray" {
    println("\t自定义对象数组")
}
```

#### 自定义对象

```swift
objectWithDictionary(value as! NSDictionary, cls: NSClassFromString(v))
```
* 运行会崩溃

> 原因分析：在Swift中，每一个类在真正使用是是有类前缀的，该前缀名就是 `namespace`

* 解决办法 -> 在自定义类的协议方法中包含命名空间

```swift
static func customClassMapping() -> [String : String]? {
    let ns = "字典转模型."
    return ["info": "\(ns)Info", "other": "\(ns)Info", "others": "\(ns)Info"]
}
```

* 代码实现

```swift
if let subObj: AnyObject = objectWithDictionary(value as! NSDictionary, cls: NSClassFromString(v)) {
    obj.setValue(subObj, forKey: k)
}
```

#### 自定义对象数组

##### 定义函数

```swift
///  创建自定义对象数组
///
///  :param: NSArray 字典数组
///  :param: cls     模型类
///
///  :returns: 模型数组
func objectsWithArray(array: NSArray, cls: AnyClass) -> NSArray? {

    return nil
}
```

##### 代码实现

```swift
///  创建自定义对象数组
///
///  :param: NSArray 字典数组
///  :param: cls     模型类
///
///  :returns: 模型数组
func objectsWithArray(array: NSArray, cls: AnyClass) -> NSArray? {

    // 遍历数组内容
    var list = [AnyObject]()

    for value in array {
        let type = "\(value.classForCoder)"

        if type == "NSDictionary" {
            if let subObj: AnyObject = objectWithDictionary(value as! NSDictionary, cls: cls) {
                list.append(subObj)
            }
        } else if type == "NSArray" {
            if let subObj: AnyObject = objectsWithArray(value as! NSArray, cls: cls) {
                list.append(subObj)
            }
        }
    }

    if list.count > 0 {
        return list
    } else {
        return nil
    }
}
```

字典转模型代码调用

```swift
if let subObj: AnyObject = objectsWithArray(value as! NSArray, cls: NSClassFromString(v)) {
    obj.setValue(subObj, forKey: k)
}
```

##### 测试代码

```swift
let dict = loadJSON() as! NSDictionary
println(dict)

let obj = manager.objectWithDictionary(dict, cls: Model.self) as? Model
println(obj?.debugDescription)
println(obj?.info?.debugDescription)
if obj?.other != nil {
    for o in obj!.other! {
        println(o.debugDescription)
    }
}
if obj?.others != nil {
    for o in obj!.others! {
        println(o.debugDescription)
    }
}

if obj?.demo != nil {
    for o in obj!.demo! {
        for oo in o as! NSArray {
            println(oo.debugDescription)
        }
    }
}
```

* 测试模型

```swift
class Model: NSObject, DictModelProtocol {
    var str1: String?
    var str2: NSString?
    var b: Bool = true
    var i: Int = 0
    var f: Float = 0
    var d: Double = 0
    var num: NSNumber?
    var namelist: [String]?
    var info: Info?
    var other: [Info]?
    var others: NSArray?
    var demo: NSArray?

    static func customClassMapping() -> [String : String]? {
        let ns = "字典转模型."
        return ["info": "\(ns)Info", "other": "\(ns)Info", "others": "\(ns)Info", "demo": "\(ns)Info"];
    }
}
```

* 测试 JSON

```json
{
    "str1": "string 001",
    "str2": "string 002",
    "b": true,
    "i": 100000,
    "i8": 8,
    "i16": 16,
    "i32": 32,
    "i64": 64,
    "fff": 10.001,
    "d": null,
    "num": null,
    "namelist": ["zhangsan", "lisi", "wangwu"],
    "info": {"name": "来啊"},
    "other": [{"name": "zhangsan"},
            {"name": "lisi"}],
    "others": [{"name": "iPhone"},
              {"name": "iOS"}],
    "demo": [[{"name": "demo1"},
              {"name": "demo2"}],
             [{"name": "omed1"},
              {"name": "omed2"}]
    ],
    "news": {
        "tlist":[
                 {"tname": "头条", "ename": "iosnews"},
                 {"tname": "社会", "ename": "shehui"}
                 ],
        "cname": "头条",
        "cid": 12345678
    },
    "newslist": [{
        "tlist":[
                 {"tname": "头条", "ename": "iosnews"},
                 {"tname": "社会", "ename": "shehui"}
                 ],
        "cname": "头条",
        "cid": 12345678
    },
     {
     "tlist":[
              {"tname": "头条头条", "ename": "iosnews"},
              {"tname": "社会社会", "ename": "shehui"}
              ],
     "cname": "社会头条",
     "cid": 12345678
     }
     ]
}
```
