# 保存 AccessToken

## 归档 & 解档

> 如果写了归档和接档方法，至少需要有一个构造函数

> 解档方法不能写在 `extension` 中

```swift
///  归档方法
func encodeWithCoder(encoder: NSCoder) {
    encoder.encodeObject(access_token)
    encoder.encodeObject(expires_in)
    // 如果是基本数据类型，需要指定 key
    encoder.encodeObject(uid)
}

///  解档方法，NSCoding 需要的方法 － required 的构造函数不能写在 extension 中
///  覆盖构造函数
required init(coder decoder: NSCoder) {
    access_token = decoder.decodeObject() as? String
    expires_in = decoder.decodeObject() as? NSNumber
    uid = decoder.decodeObject() as? NSNumber
}
```

## 保存 & 加载

```swift
///  将数据保存到沙盒
func saveAccessToken() {
    NSKeyedArchiver.archiveRootObject(self, toFile: AccessToken.tokenPath())
}

///  从沙盒读取 token 数据
class func loadAccessToken() -> AccessToken? {
    return NSKeyedUnarchiver.unarchiveObjectWithFile(tokenPath()) as? AccessToken
}

///  返回保存在沙盒的路径
class func tokenPath() -> String {
    var path = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true).last as! String
    path = path.stringByAppendingPathComponent("WBToken.plist")

    return path
}
```


