# 字典转模型

* 新建项目 `SwiftDictModel`，添加到 `workspace`
* 将备课代码的字典转模型拖入到项目中

在 `DemoViewController` 中添加以下代码测试字典转模型

```swift
import SwiftDictModel

///  加载用户信息
@IBAction func loadUserInfo() {
    let urlString = "https://api.weibo.com/2/users/show.json"
    let params = ["access_token": AccessToken.accessToken!.access_token!,
        "uid": "\(AccessToken.accessToken!.uid)"]

    net.requestJSON(.GET, urlString, params) { (result, error) -> () in
        self.saveToFile(result as! NSDictionary, fileName: "user.json")

        let manager = SwiftDictModel.sharedManager
        var user = manager.objectWithDictionary(result as! NSDictionary, cls: UserInfo.self) as! UserInfo
        var dict = manager.objectDictionary(user)
        println(dict)
    }
}
```
