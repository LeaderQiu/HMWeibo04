# 用户信息

## 显示用户信息

```swift
///  extension 是一个分类，分类不允许有存储能力
///  如果要打印对象信息，OC 中的 description，在 swift 中需要遵守协议 DebugPrintable
extension AccessToken: DebugPrintable {

    override var debugDescription: String {
        let dict = self.dictionaryWithValuesForKeys(["access_token", "expiresDate", "uid"])
        return "\(dict)"
    }
}
```

## 调整归档&解档函数

> 修改 uid 属性

```swift
var uid : Int = 0
```

> 调整归档解档函数

```swift
///  归档方法
func encodeWithCoder(encoder: NSCoder) {
    encoder.encodeObject(access_token)
    encoder.encodeObject(expiresDate)
    // 如果是基本数据类型，需要指定 key
    encoder.encodeInteger(uid, forKey: "uid")
}

///  解档方法，NSCoding 需要的方法 － required 的构造函数不能写在 extension 中
///  覆盖构造函数
required init(coder decoder: NSCoder) {
    access_token = decoder.decodeObject() as? String
    expiresDate = decoder.decodeObject() as? NSDate
    uid = decoder.decodeIntegerForKey("uid")
}
```


