# 网络框架

## 目标

1. 最常用的获取数据的方式 `GET`/`POST`
2. 最常见的数据类型: `JSON`/`Image`/`二进制数据`

## 定义函数

> 从网络上加载 JSON，完成反序列化的工作！

```swift
// 定义闭包类型，类型别名－> 首字母一定要大写
public typealias Completion = (result: AnyObject?, error: NSError?) -> ()

///  请求 JSON
///
///  :param: method     HTTP 访问方法
///  :param: urlString  urlString
///  :param: params     可选参数字典
///  :param: completion 完成回调
public func requestJSON(method: HTTPMethod, _ urlString: String, _ params: [String: String]?, completion: Completion) {
}
```
