# Token 过期判断

## 判断 Token 过期

> 用户登录成功获取 `token` 之后，从服务器返回的 `expires_in` 是用户第一次获取 `token` 到过期的时间。如果用户不再登录，则无法根据 `expires_in` 判断 `token` 过期日期

* 解决办法：添加 `expiresDate` 并且在 `expires_in` 属性的 `didSet` 中计算过期日期

```swift
///  如果不是在有效期，需要让用户重新登录！
var expires_in: NSNumber? {
    didSet {
        expiresDate = NSDate(timeIntervalSinceNow: expires_in!.doubleValue)
        println("过期日期 \(expiresDate)")
    }
}

///  token过期日期
var expiresDate: NSDate?
```

* 添加 `isExpired` 属性辅助判断 `Token` 是否过期

```swift
/// 是否过期－用过期日期和当前时间进行比较
var isExpired: Bool {
    return expiresDate?.compare(NSDate()) == NSComparisonResult.OrderedAscending
}
```
