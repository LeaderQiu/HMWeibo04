# 加载微博数据

## 通过类函数返回微博数据

### 添加类函数

在 `Statuses` 类中添加类函数如下：

```swift
///  加载微博数据
static func loadStatus(completion: NetworkManager.Completion) {

}
```

### 定义常量

```swift
/// 获取当前登录用户及其所关注用户的最新微博
private let WB_Home_Timeline_URL = "https://api.weibo.com/2/statuses/home_timeline.json"
```

### 扩展 AccessToken

为了避免每次都从沙盒加载 `AccessToken`，在 `AccessToken` 中增加类属性

```swift
private static var _accessToken: AccessToken?
/// 类变量 accessToken，从沙盒加载 token 记录
class var accessToken: AccessToken? {
    if _accessToken == nil {
        _accessToken = loadAccessToken()
    }
    return _accessToken
}
```
> 在 swift 中，不允许在类属性保存实例，不过可以通过类属性返回某一个内部的静态成员

### 代码实现

```swift
///  加载微博数据
static func loadStatus(completion: NetworkManager.Completion) {

    if let token = AccessToken.accessToken?.access_token {
        let net = NetworkManager.sharedManager
        let dictManager = DictModelManager.sharedManager
        let params = ["access_token": token]

        net.requestJSON(.GET, WB_Home_Timeline_URL, params) { (result, error) -> () in

            if let dict = result as? NSDictionary {
                // 字典转模型
                let statuses = dictManager.objectWithDictionary(dict, cls: Statuses.self) as! Statuses

                completion(result: statuses, error: error)
            }
        }
    }
}
```

## 视图控制器中加载微博数据

```swift
///  加载微博数据
func loadStatuses() {
    SVProgressHUD.show()

    Statuses.loadStatus { (result, error) -> () in
        if error != nil {
            SVProgressHUD.showInfoWithStatus("网络不给力")
        } else if result != nil {
            SVProgressHUD.dismiss()

            self.statuses = result as? Statuses
            self.tableView.reloadData()
        }
    }
}
```

### 表格数据源方法

```swift
override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.statuses?.statuses?.count ?? 0
}

override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("HomeCell", forIndexPath: indexPath) as! UITableViewCell

    let status = self.statuses!.statuses![indexPath.row]
    cell.textLabel?.text = status.text

    return cell
}
```

> 体会：`业务模型处理数据`，`视图控制器处理数据&视图的交互`

### 细节处理

#### 修改 `SimpleNetwork`

1> 将所有网络回调都放在主线程，简化后续代码操作

```swift
dispatch_async(dispatch_get_main_queue()) { () -> Void in
    // 如果有错误，直接回调，将网络访问的错误传回
    if error != nil {
        completion(result: nil, error: error)
        return
    }

    // 反序列化 -> 字典或者数组
    let json: AnyObject? = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: nil)

    // 判断是否反序列化成功
    if json == nil {
        let error = NSError(domain: SimpleNetwork.errorDomain, code: -1, userInfo: ["error": "反序列化失败"])
        completion(result: nil, error: error)
    } else {
        completion(result: json, error: nil)
    }
}
```

2> 在 `completion` 参数前增加 _

```swift
public func requestJSON(method: HTTPMethod, _ urlString: String, _ params: [String: String]?, _ completion: Completion)
```

#### 修改 `NetworkManager` 等相关网路调用

1> 在 `completion` 参数前增加 _，并且添加函数说明

```swift
///  请求 JSON
///
///  :param: method     HTTP 访问方法
///  :param: urlString  urlString
///  :param: params     可选参数字典
///  :param: completion 完成回调
func requestJSON(method: HTTPMethod, _ urlString: String, _ params: [String: String]?, _ completion: Completion) {

    net.requestJSON(method, urlString, params, completion)
}
```

2> 修改 `OAuthViewController` 中的网络请求调用，删除 `completion`

3> 删除 `OAuthViewController` 中过期的注释





