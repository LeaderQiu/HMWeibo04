# Workspace

1. 创建 `Workspace` -> `HMWeibo04.xcworkspace`
2. 将 `HMWeibo04` 拖拽到 工作组
3. 新建项目选择 `Framework & Library` -> `Cocoa Touch Framework`
4. 将项目名称命名为 `SimpleNetwork`
5. `Add To` 和 `Group` 都选择 `HMWeibo04`
6. 在 `SimpleNetwork` 目录下新建 `Sources` 目录，用于保存框架文件，之所以建立单独的目录，是为了方便开源框架使用
7. 新建 `SimpleNetwork.swift` 作为网络框架类

```swift
public class SimpleNetwork {
}
```


