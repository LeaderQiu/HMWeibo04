# 请求参数

> 由于 GET & POST 拼接方式是一样的，以值对的方式表示参数，多个参数之间使用 `&` 连接

```swift
///  生成查询字符串
///
///  :param: params 可选字典
///
///  :returns: 拼接完成的字符串
func queryString(params: [String: String]?) -> String? {

    // 0. 判断参数
    if params == nil {
        return nil
    }

    // 1. 定义一个数组
    var array = [String]()
    // 2. 遍历字典
    for (k, v) in params! {
        let str = k + "=" + v.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        array.append(str)
    }

    return join("&", array)
}
```
