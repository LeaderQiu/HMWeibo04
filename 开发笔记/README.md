# 新浪微博开发笔记

## iPhone 项目目标

* 项目掌控能力
* 工具使用能力
* 开发技巧能力

## 应用程序信息

| 名称 | 说明 |
| -- | -- |
| AppName | 飞得更高啊 |
| App Key | 2720451414 |
| App Secret | e061ff9a264a0bedb55226685b34c084 |
| redirect_uri | http://www.itheima.com |

OAuth 完整测试地址 URL

[https://api.weibo.com/oauth2/authorize?client_id=2720451414&redirect_uri=http://www.itheima.com](https://api.weibo.com/oauth2/authorize?client_id=2720451414&redirect_uri=http://www.itheima.com)

## 新浪微博接口地址

* 微博开放平台地址
[http://open.weibo.com](http://open.weibo.com)

* 微博接口文档地址
[http://open.weibo.com/wiki/微博API](http://open.weibo.com/wiki/微博API)

