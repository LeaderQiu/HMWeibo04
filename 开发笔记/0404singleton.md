# 单例&模型缓存

目前存在的问题

* 字典转模型在实际开发中会非常频繁
* 如果每次都用运行时解析一次类的信息会造成性能的浪费

解决办法

* 建立一个单例统一负责所有的字典转模型工作
* 同时单例中建立一个模型缓冲池，存放所有解析过的模型

## 单例

```swift
private static let instance = SwiftDictModel()
/// 全局统一访问入口
class var sharedManager: SwiftDictModel {
    return instance
}
```

## 模型缓存

```swift
/// 模型缓冲，[类名: 模型信息字典]
var modelCache = [String: [String: String]]()
```

读取缓冲池

```swift
// 检测缓冲池
if let cache = modelCache["\(cls)"] {
    println("\(cls) 已经被缓存")
    return cache
}
```

写入缓冲池
```swift
modelCache["\(cls)"] = infoDict
```


