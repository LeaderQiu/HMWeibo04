# 用户账户

## 定义模型

```swift
class AccessToken: NSObject, NSCoding {
    ///  用于调用access_token，接口获取授权后的access token。
    var access_token: String?
    ///  access_token的生命周期，单位是秒数。
    ///  从微博服务器返回的 token 是有有效期的
    ///  如果是开发者自己，时间是 5 年，如果是其他用户，时间是 2/3 天
    var expires_in: NSNumber?
    ///  access_token的生命周期（该参数即将废弃，开发者请使用expires_in）。
    var remind_in: NSNumber?
    ///  当前授权用户的UID，可以通过这个 id 获取用户的进一步信息
    var uid: NSNumber?
}
```

## 构造函数

> 一旦自定义了构造函数，默认的 `init` 会被忽略

```
///  构造函数，一旦写了，init 会被忽略
init(dict: NSDictionary) {
    super.init()

    self.setValuesForKeysWithDictionary(dict as [NSObject : AnyObject])
}
```

