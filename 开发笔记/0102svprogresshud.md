# SVProgressHUD

* 将 `SVProgressHUD` 拖拽到 `Libs` 目录中
* 在 `HMWeibo` 的 `Supporting Files` 下新建 `HMWeibo-Bridge.h`
* 在 `HMWeiboTests` 的 `Supporting Files` 下新建 `HMWeiboTests-Bridge.h`
* 在两个 .h 中都添加以下引入语句

```
#import "SVProgressHUD.h"
```

* 在 TARGETS 中`两个`项目的 `Build Settings` 中搜索 `bridg`
* HMWeibo 项目的 `Objective-C Bridging Header` 中输入
```
HMWeibo/HMWeibo-Bridge.h
```
* HMWeiboTests 项目的 `Objective-C Bridging Header` 中输入
```
HMWeiboTests/HMWeiboTests-Bridge.h
```

说明：

> 之所以要在单元测试中同样使用桥接文件，是因为单元格式测试的文件中会调用这些第三方框架！

## cocoapods

`cocoapods` 目前最新的测试版是 `0.36.0.rc.1`，正式版是 `0.35`

如果要在 `swift` 中使用 `cocoapods` 需要使用 `0.36` 的版本才可以

> 0.36.bata 能够支持中文，但是如果使用 0.36.0.rc，项目中如果有中文会出现下图的效果

![](./images/蛋疼的pod3.6.png)




