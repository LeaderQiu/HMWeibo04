# OAuth 界面

## 添加文件

* OAuthViewController.swift
* Main.storyboard 重命名为 OAuth.storyboard
* 修改启动选项
* 删除网络工具获取 Token 的代码
* 运行测试，会发现只有点击 "取消按钮" 时需要重新加载授权页面

## 恢复单元测试

* OAuthViewControllerTests.swift
* 修改 OAuthViewController & OAuth 的 Target
* 修改 oauthVC 懒加载中的 sb 名称
* 运行测试

* 针对新的问题扩展方法 -> 只有点击取消，才需要重新加载授权页面

1> 增加函数返回值

```swift
func continueWithCode(url: NSURL) -> (load: Bool, code: String?, reload: Bool)
```

2> 修改返回代码

3> `cmd + u` 重新运行单元测试

4> 添加单元测试

```swift
///  测试重新加载授权页面
func testReloadAuthPage() {
    // 取消授权
    let url = NSURL(string: "http://www.itheima.com/?error_uri=%2Foauth2%2Fauthorize&error=access_denied&error_description=user%20denied%20your%20request.&error_code=21330")!
    let result = oauthVC!.continueWithCode(url)
    XCTAssertFalse(result.load, "不应该加载")
    XCTAssertNil(result.code, "不应该有code")
    XCTAssertTrue(result.reload, "应该加载授权页面")
}
```

5> 代码实现

```swift
// 3. 如果是回调地址，需要判断 code
if urlString.hasPrefix(WB_Redirect_URL_String) {
    if let query = url.query {
        let codestr: NSString = "code="

        if query.hasPrefix(codestr as String) {
            var q = query as NSString!
            return (false, q.substringFromIndex(codestr.length), false)
        } else {
            return (false, nil, true)
        }
    }
}
```

* 调整重新加载授权页面代码

```swift
if !result.load && result.reload {
    // 重新刷新授权页面
    loadAuthPage()
}
```
