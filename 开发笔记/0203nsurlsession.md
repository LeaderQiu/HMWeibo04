# NSURLSession

```swift
///  公共构造函数
public convenience init () {
    let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
    self.init(configuration: configuration)
}

///  构造函数
///
///  :param: timeoutInterval 超时时长
public convenience init(timeoutInterval: NSTimeInterval) {
    let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
    configuration.timeoutIntervalForRequest = timeoutInterval
    configuration.timeoutIntervalForResource = timeoutInterval

    self.init(configuration: configuration)

    // 注意，在调用默认构造函数之前，不能在convenience构造函数中使用属性
    self.timeoutInterval = timeoutInterval
}

///  默认构造函数
public init(configuration: NSURLSessionConfiguration) {
    self.session = NSURLSession(configuration: configuration)
}

///  网络会话
var session: NSURLSession?
///  默认网络超时时长
var timeoutInterval: NSTimeInterval?
```
