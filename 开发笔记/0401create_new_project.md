# 新建项目

## 项目搭建

* 新建单视图应用 `字典转模型`
* 新建项目 `SwiftDictModel`，Add to & Group 都选择 `字典转模型`
* 在 `SwiftDictModel` 文件夹下创建 `Sources` 目录，拖拽到项目中，注意不要选择 `Copy items if needed`
* 新建 `SwiftDictModel.swift` 文件负责字典转模型功能实现

```swift
public class SwiftDictModel {
    public init() {}
}
```

## 准备测试数据

在`字典转模型`目录下新建两个目录 `DataFile` & `Model` 分别存放模型和 JSON 数据

### 加载 JSON
```swift
func loadJSON() {
    let path = NSBundle.mainBundle().pathForResource("Model01.json", ofType: nil)
    let data = NSData(contentsOfFile: path!)!
    let json = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.allZeros, error: nil) as? NSDictionary
    println(json)
}
```

### 准备模型

```swift
///  测试模型
class Model: NSObject {
    var str1: String?
    var str2: NSString?
    var b: Bool?
    var i: Int?
    var f: Float?
    var d: Double?
    var num: NSNumber?
    var namelist: [String]?
    var info: Info?
    var other: [Info]?
    var others: NSArray?
}

class Info: NSObject {
    var name: String?
}
```





