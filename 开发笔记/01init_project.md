# 项目搭建

* 创建项目加入 git.oschina.net

## 搭建主题框架

* 搭建主体框架目录

| 目录名 | 说明 |
| -- | -- |
| Libs | 第三方框架 |
| Classes | 主程序目录 |

* 主程序目录结构

| 目录名 | 说明 |
| -- | -- |
| UI | 功能界面 |
| BusinessModel | 业务模型 |
| Tools | 工具 |

* UI 目录结构

| 目录名 | 说明 |
| -- | -- |
| OAuth | 授权 |
| Main | 主功能 |
| Home | 首页 |
| Message | 消息 |
| Discover | 发现 |
| Profile | 我 |

* 删除模板提供的文件
    * LaunchScreen.xib
    * Main.storyboard
    * ViewController.swift
